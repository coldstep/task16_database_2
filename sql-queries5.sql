#(Підзапити у якості обчислювальних стовпців)
#1
SELECT part1.maker, part1.PC, part1.laptop, part2.printer
FROM
(
	SELECT A.maker, A.PC, B.laptop
	FROM
		(
			SELECT maker, count(pc_temp.model) as 'PC'
			FROM product
			LEFT JOIN 
				(
					SELECT model
					FROM pc
					GROUP BY pc.model
				)pc_temp
			ON product.model= pc_temp.model
			GROUP BY product.maker
		)A
	LEFT JOIN
		(
			SELECT maker, count(laptop_temp.model) as 'laptop'
			FROM product
			LEFT JOIN 
				(
					SELECT model
					FROM laptop
					GROUP BY laptop.model
					)laptop_temp
			ON product.model= laptop_temp.model
			GROUP BY product.maker
		)B
	ON A.maker=B.maker
)part1
JOIN 
(
	SELECT maker, count(printer_temp.model) as 'printer'
	FROM product
	LEFT JOIN 
		(
			SELECT model
			FROM printer
			GROUP BY printer.model
		)printer_temp
	ON product.model= printer_temp.model
	GROUP BY product.maker
)part2
ON part1.maker = part2.maker
ORDER BY part1.maker
;
#2
SELECT maker, AVG(screen) as 'середній розмір екрану'
FROM product
JOIN laptop
ON product.model = laptop.model
GROUP BY maker;
#3
SELECT maker, MAX(price) as 'максимальна ціна ПК'
FROM product
JOIN pc
ON product.model = pc.model
GROUP BY maker;
4#
SELECT maker, MIN(price) as 'мінімальна ціна ПК'
FROM product
JOIN pc
ON product.model = pc.model
GROUP BY maker;
5#
SELECT maker, AVG(a.price) as ' середня ціна'
FROM product
JOIN 
(
	SELECT model,price,speed
    FROM pc 
    WHERE speed >600
)A
ON product.model = A.model
GROUP BY maker;

