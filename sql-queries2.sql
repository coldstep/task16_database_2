#(Використання підзапитів з лог. операцією EXISTS)
#1.  
    SELECT p.maker,p.model
    FROM product p
    WHERE EXISTS (SELECT model FROM pc WHERE p.model = model);
#2.  
    SELECT p.maker
    FROM product p
    WHERE EXISTS 
                (
                    SELECT model FROM pc
                    WHERE p.model = model 
                    AND speed >=750
                );
#3. 
	SELECT  maker
    FROM product p1
    WHERE model IN 
                (
                    SELECT model
                    FROM pc
                    WHERE speed >= 750)
	AND EXISTS (
                    SELECT  maker, model
                    FROM product p2
                    WHERE model IN
                                (
                                    SELECT model 
                                    FROM laptop
                                    WHERE speed >= 750
                                )
                    AND p1.maker = p2.maker
                )
    ;
    	
#4.  
    SELECT maker
    FROM product p
    WHERE EXISTS (
                    SELECT maker 
                    FROM product
                    WHERE model = p.model
                    AND maker IN (						
                                        SELECT maker
                                        FROM product pr2
                                        WHERE pr2.model IN 
                                                        (	
                                                            SELECT model
                                                            FROM printer
                                                        )
                                    )
                        AND maker IN (
                                        SELECT maker
                                        FROM product pr1
                                        WHERE p.model = pr1.model 
                                        AND pr1.model IN 
                                                        (
                                                            SELECT model 
                                                            FROM pc
                                                            WHERE speed = (SELECT MAX(Speed) FROM pc)
                                                        )
                                    )
                
    );  
#5. 
    SELECT sh.name, sh.launched
    FROM ships sh
    WHERE EXISTS (
                    SELECT class 
                    FROM classes
                    WHERE class = sh.class
                    AND displacement >=35000
                    
                );
