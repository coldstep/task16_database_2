(Оператор CASE)
#1
SELECT A.maker,
CASE
	WHEN A.type= 'PC' THEN concat('yes(',counter,')')
    ELSE 'no'
END AS 'PC'
FROM 
(
	SELECT maker,model,type
    FROM product
    GROUP BY maker
)A
LEFT JOIN
	(
		SELECT maker,model,type , count(model) as counter
        FROM product 
        WHERE model IN 
					(
						SELECT model 
                        FROM pc
					)
		GROUP BY maker
	)B
ON A.model=B.model
ORDER BY A.maker
;
#2
SELECT 
    *
FROM
    outcome_o,
    income_o
ORDER BY (CASE
    WHEN outcome_o.date = income_o.date THEN income_o.inc
    WHEN outcome_o.date = income_o.date THEN outcome_o.out
    ELSE 'none'
END)
;