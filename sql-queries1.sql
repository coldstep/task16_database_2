#(Використ підзапитів у конструкції WHERE з викор. IN, ANY,ALL)
#1
    SELECT DISTINCT maker
	FROM product 
    WHERE maker NOT IN (SELECT maker FROM product WHERE model IN (SELECT model FROM laptop));
#2 
    SELECT DISTINCT maker , product.type
	FROM product 
    WHERE maker <> ALL (SELECT maker FROM product WHERE model IN (SELECT model FROM laptop));
#3

