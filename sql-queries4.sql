#(Статистичні функції та робота з групами)
#1.  
    SELECT model,price
    FROM printer
    WHERE price = 
        (
            SELECT MAX(price) 
            FROM printer
        );
#2.   
    SELECT p.type, l.model, l.speed
	 FROM laptop l
	 JOIN product p
	 ON l.model = p.model
	 WHERE speed < (SELECT MIN(speed) FROM pc);
#3.   
    SELECT 'the chipest color printer' as temp, p.maker, pr.model, pr.price
	 FROM printer pr
	 JOIN product p
	 ON pr.model = p.model
	 WHERE pr.color = 'y'
     AND pr.price = (SELECT MIN(price) FROM printer WHERE color = 'y' );
#4.  
    SELECT s2.maker, s2.count
    FROM (
            SELECT s1.maker,s1.type,s1.model, COUNT(s1.maker) as 'count'
	        FROM 
            (
                SELECT p.model,p.maker,p.type
                FROM  product p
                WHERE p.type = 'PC'
                GROUP BY p.maker,p.model
            )s1
            group by s1.maker 
        )s2
    WHERE s2.count>=2;
#5.  
    SELECT CONCAT('середній розмір жорсткого диску - ',temp.average) AS CONCAT
    FROM(
        SELECT AVG(hd) AS average
        FROM pc
        WHERE pc.model IN(
                            SELECT p.model
                            FROM product p
                            WHERE p.model IN (
                                                SELECT model 
                                                FROM pc
                                            )
                            AND p.maker IN (
                                                SELECT maker 
                                                FROM product 
                                                WHERE model IN (
                                                                    SELECT model 
                                                                    FROM printer
                                                                )
                                            )
                        )
        )temp
    ;
