﻿DROP DATABASE IF EXISTS epam_homework2;
CREATE DATABASE epam_homework2
CHARACTER SET utf8
COLLATE utf8_general_ci;
USE epam_homework2;

#---scholarship--------------------------------------------------------------------------------------------------------
CREATE TABLE scholarship
(	
	id_scholarship int AUTO_INCREMENT PRIMARY KEY,
	scholarship_level varchar(45) NOT NULL,
    scholarship_amount double NOT NULL
);

INSERT INTO scholarship(scholarship_level, scholarship_amount)
VALUES ('none',0.0),
	('standart',1000.00),
    ('high',1500.00),
	('president',5000.00);
    
#---university_group--------------------------------------------------------------------------------------------------------        
CREATE TABLE university_group
(
 	id_group int AUTO_INCREMENT PRIMARY KEY,
	group_name varchar(45) NOT NUll
);

INSERT INTO university_group(group_name)
VALUE ('JAVA LAB 21'),
	('JAVA LAB 22'),
    ('JAVA LAB 23'),
    ('JAVA LAB 24'),
    ('JAVA LAB 25');

#---university_group--------------------------------------------------------------------------------------------------------        
CREATE TABLE speciality
(	
	id_speciality int AUTO_INCREMENT PRIMARY KEY,
	speciality_name varchar(100) NOT NULL
);

INSERT INTO speciality (speciality_name)
VALUES ('swift developer'),
	('HR manager'),
    ('java developer'),
    ('php developer'),
    ('full stack developer');

#---teacher--------------------------------------------------------------------------------------------------------  
CREATE TABLE teacher
(	
	id_teacher int AUTO_INCREMENT PRIMARY KEY,
	surname varchar(45) NOT NULL,
	name varchar(45) NOT NULL,
    middle_name varchar(45) NOT NULL
);

INSERT INTO teacher (surname, name, middle_name)
VALUES ('Джобс','Стів', 'Андрійович'),
	('Квіл','Пітер','Петрович'),
    ('Саакашвілі','Олена','Ілівна'),
    ('Порошенко','Олександр','Сергійович'),
    ('Стетхем','Володимир','Юрійович');

#---discipline_has_teacher--------------------------------------------------------------------------------------------------------  
CREATE TABLE discipline
(
	id_discipline int AUTO_INCREMENT PRIMARY KEY,
    discipline_name varchar(100) NOT NULL,
    type_of_control varchar(45) NOT NULL
);

INSERT INTO discipline (discipline_name,type_of_control)
VALUES ('Information technology','exam'),
	('Line algebra','score'),
    ('CAD','exam'),
    ('Operation system','score'),
    ('Philosophy','exam');

#---discipline_has_teacher--------------------------------------------------------------------------------------------------------  
CREATE TABLE discipline_has_teacher
(
	id_discipline int,
    id_teacher int,
    PRIMARY KEY (`id_discipline`, `id_teacher`),
    CONSTRAINT FK_disciple_has_teacher_id_discipline
		FOREIGN KEY (id_discipline)
	REFERENCES discipline(id_discipline)
    ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT FK_discipline_has_teacher_id_teacher
		FOREIGN KEY (id_teacher)
	REFERENCES teacher(id_teacher)
    ON UPDATE CASCADE ON DELETE CASCADE
);

INSERT INTO discipline_has_teacher(id_discipline,id_teacher)
VALUES (1,5),
	(2,2),
    (3,1),
    (1,3),
    (1,2),
    (3,4),
    (5,1);
    
#---student--------------------------------------------------------------------------------------------------------  
CREATE TABLE student
(
	id_student int AUTO_INCREMENT PRIMARY KEY,
    surname varchar(50) NOT NULL,
    name varchar(50) NOT NULL,
    photo varchar(100), 
    autobiography varchar(200),
    id_group int NOT NULL,
    id_speciality int NOT NULL,
    id_scholarship int NOT NULL,
    entry_date date NOT NULL,
    date_of_birth date NOT NULL,
    address varchar(100),
    curent_rating double,
    CONSTRAINT FK_university_group
		FOREIGN KEY (id_group)
	REFERENCES university_group(id_group)
    ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT FK_speciality
		FOREIGN KEY (id_speciality)
	REFERENCES speciality(id_speciality)
    ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT FK_scholarship
		FOREIGN KEY (id_scholarship)
	REFERENCES scholarship (id_scholarship)
    ON UPDATE CASCADE ON DELETE CASCADE
);

INSERT INTO student
		(
			surname,
            name,
            photo,
            autobiography,
            id_group,
            id_speciality,
            id_scholarship,
            entry_date,
            date_of_birth,
            address,
            curent_rating
		)
VALUES
	('Zukerberg','Mark','/src/main/resources/photo/ZukerbergMark','none',1,5,4,'1990-09-01','1970-02-03','San Francisco',5.0),
	('Gates','Bill','/src/main/resources/photo/GatesBill','none',2,1,4,'1980-09-01','1960-05-03','Los Angeles',5.0),
    ('Stark','Tony','/src/main/resources/photo/StarkTony','none',3,5,4,'1995-09-01','1980-03-03','New York',5.0),
    ('Klatte','Susanne','/src/main/resources/photo/KlatteSusanne','none',4,2,3,'1980-09-01','1960-02-03','Berlin',5.0),
    ('Gosling','James','/src/main/resources/photo/GoslingJames','none',5,1,1,'1970-09-01','1950-02-03','Miami',5.0)
;

#---list_Of_studied_discipline--------------------------------------------------------------------------------------------------------  

CREATE TABLE list_Of_studied_discipline
(	
	id_student int,
    id_discipline int,
    number_of_semestri int,
    module_1_points double,
    module_2_points double,
    semestri_points_in_hundred_level_scale double,
    semestri_points_in_fife_level_scale int,
    PRIMARY KEY (`id_student`, `id_discipline`),
    CONSTRAINT FK_student_list
		FOREIGN KEY (id_student)
	REFERENCES student(id_student)
    ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT FK_discipline_list
		FOREIGN KEY (id_discipline)
	REFERENCES discipline(id_discipline)
);

INSERT INTO list_of_studied_discipline
		(
			id_student,
            id_discipline,
            number_of_semestri,
            module_1_points,
            module_2_points,
            semestri_points_in_hundred_level_scale,
            semestri_points_in_fife_level_scale
		)
VALUES
	(1,2,4,44.0,44.0,88.0,5),
	(2,5,4,42.0,43.0,85.0,4),
    (3,1,4,32.0,40.0,72.0,4),
    (2,3,4,20.0,44.0,64.0,3),
    (1,3,3,44.0,44.0,88.0,5),
    (4,2,4,50.0,50.0,100.0,5),
    (2,1,2,44.0,44.0,88.0,5),
    (3,2,4,44.0,44.0,88.0,5)
;