#(Конкатенація стрічок чи мат. обчислення чи робота з датами)
#1.  
    SELECT CONCAT("середня ціна =") AS text,AVG(price) as average
    FROM laptop;
#2.  
    SELECT CONCAT('модель: ',model) as model, CONCAT('ціна: ',price) as price
    FROM pc;
#3.  
    SELECT CONCAT( YEAR(date),'.',MONTH(date),'.',DAY(date)) as 'рік.число_місяця.день'
    FROM income;
#4.  
    SELECT ship,battle,
        CASE result
        WHEN 'OK' THEN 'цілий неушкоджений'
        WHEN 'damaged' THEN 'поранений'
        WHEN 'sunk' THEN 'потоплений'
        ELSE 'default'
        END AS result
    FROM outcomes;
#5.  
    SELECT	pit.trip_no, 
            pit.date, 
            pit.ID_psg, 
            CONCAT('ряд: ',SUBSTRING(pit.place,1,1)) AS line, 
            CONCAT('місце: ',SUBSTRING(pit.place,2,1)) AS place
    FROM pass_in_trip pit;
#6.  
    SELECT  t.trip_no,
            t.ID_comp,
            t.plane, 
            CONCAT('from ',t.town_from,' to ',town_to) as route,
            t.time_out,
            t.time_in
    FROM trip t;